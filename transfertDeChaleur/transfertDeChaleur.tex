%Fabian Huber
%T.IS-E2A
%10/02/2021
\documentclass[a4paper, 12pt]{article}
\usepackage[french]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[a4paper]{geometry}
\usepackage{fancyhdr}
\usepackage{lastpage}
\usepackage{hyperref}
\usepackage{graphicx}
\graphicspath{ {./img/} }
\usepackage{mathtools} % math equations
\usepackage{textcomp} % loaded by gensymb
\usepackage{gensymb} % for the degree symbol
\usepackage{amsmath}
\usepackage{lmodern}

\pagestyle{fancy}

\setlength\parskip{5pt}
\setlength{\parindent}{0pt}
\setlength{\headheight}{15pt}

% Header and Footer
\lhead{Transfert thermique - Dilatation thermique}
\chead{}
\rhead{\today}

\lfoot{Version 1.2}
\cfoot{}
\rfoot{Page \thepage{} sur~\pageref{LastPage}}

\begin{document}
%*************
%Titre
%*************
\begin{center}
    \LARGE
    Transfert thermique - Dilatation thermique
\end{center}

\tableofcontents

\section{Types de transferts de chaleur}

Il y a 3 façons dont la chaleur se propage :

\begin{enumerate}
    \item \texttt{Conduction} Dans ce cas la chaleur se propage dans l'objet. Se produit dans les liquides et les solides.
    \item \texttt{Convection} La chaleur se transmet par déplacement de matière. Se produit dans les liquides et les gaz.
    \item \texttt{Rayonnement} La matière rayonne et emmet sa chaleur. Se produit même dans le vide. 
    C'est comme ça que le soleil nous chauffe par exemple.
    Ce document n'explique pas le rayonnement.
\end{enumerate}


\section{Conduction}

\subsection{Théorie}

La conduction est le passage de la chaleur d'un point à un autre d'un corps sous l'action d'une différence de température.
Cela se produit principalement dans les solides, mais également un petit peu dans les liquides et les gaz.
Chaque matériel à faculté de conduire la chaleur différente. On note cette valeur $ \lambda $ (se dit lambda).

Pour calculer la chaleur qui se transmet grâce à la conduction on peut utiliser cette équation :

\begin{equation} \label{eq:1}
Q = S \cdot \frac{\lambda}{e} \cdot (T_{1} - T_{2}) \cdot \Delta t
\end{equation}

Mais cela ne permet d'y calculer que pour un matériau. Pour utiliser plusieurs matériaux on utilise cette équation :

\begin{equation} \label{eq:2}
Q = S \cdot \frac{1}{R} \cdot (T_{1} - T_{2}) \cdot \Delta t
\end{equation}

Ou $R$ = La constante de résistivité.

\begin{equation} \label{eq:3}
R = \frac{e_{1}}{\lambda_{1}} + \frac{e_{2}}{\lambda_{2}} + \cdots
\end{equation}

avec :

\begin{itemize}
    \item[$Q$ :] La quantité d'énergie conduite par l'objet en $[J]$.
    \item[$S$ :] Surface de l'objet en $[m^2]$.
    \item[$\lambda$ :] Constante de conductivité thermique du matériau en $[\frac{W}{m \cdot K}]$.
    \item[$e$ :] Épaisseur de l'objet en $[m]$.
    \item[$T_{1}$ :] Température d'un côté de l'objet en $[K]$ ou $\degree C$.
    \item[$T_{2}$ :] Température de l'autre côté de l'objet en $[K]$ ou $\degree C$.
    \item[$\Delta t$ :] Durée de la conductivité en $[s]$.
\end{itemize}

\subsection{Exercice}

\subsubsection{Énoncé}

Avant, les fenêtres étaient juste constituée d'une couche de vitre. Aujourd'hui, elle sont constituée d'une couche de vitre, d'une couche d'air et d'une couche de vitre. On appelle ça le double vitrage. Comparez la conduction thermique d'une fenêtre simple et d'un double vitrage.

Variables "globales" :

\[\lambda_{verre} = 0,72 [\frac{W}{m \cdot K}]\]
\[\lambda_{air} = 0,025 [\frac{W}{m \cdot K}]\]
\[\Delta t = 60 [s]\]
\[S = 2 [m^2]\]
\[T_{1} = 20 \degree C\]
\[T_{2} = -5 \degree C\]
\[S = 2 [m^2]\]

Voici les variables connues de la fenêtre simple :

\[e = 1 [cm]\]

Voici les variables connues du double vitrage :

\[e_{verre1} = 3 [mm]\]
\[e_{air} = 3 [mm]\]
\[e_{verre2} = 3 [mm]\]

\subsubsection{Correction}

On vas commencer en calculant l'énergie de la vitre simple.
Comme il n'y a qu'un matériau, on peut utiliser l'équation \ref{eq:1}.
On convertis les $1[cm]$ en $0,01[m]$ avant tout.
On peut ensuite remplir l'équation.

\[Q = 2 \cdot \frac{0,72}{0,1} \cdot (20 - (-5)) \cdot 60 = 216'000[J]\]

On calcule ensuite l'énergie du double vitrage avec l'équation \ref{eq:2}.
Il faut dont d'abord calculer la constante de résistivité avec l'équation \ref{eq:3}.

\[R = \frac{0,003}{0,72} + \frac{0,004}{0,025} + \frac{0,003}{0,72} = \frac{101}{600}[\frac{m^2 \cdot K}{W}]\]
\[Q = 2 \cdot \frac{1}{101/600} \cdot (20 - (-5)) \cdot 60 = 17'821,8[J]\]

La vitre simple laisse donc passer 198'178,2 joules de plus que le double vitrage.

\section{Convection}

\subsection{Théorie}

La convection est le transfert de chaleur par dépalcement de matière.
Cela ne se produit que dans les liquides et les gaz.
Chaque matériau à une capacité propre de transfert de chaleur par ce biais. On notte cette valeur $ \alpha $ (alpha).

Pour calculer la chaleur qui se transmet grâce à la convection, on peut utiliser cette équation :

\begin{equation} \label{eq:4}
    Q = S \cdot \alpha \cdot (T_{1} - T_{2}) \cdot \Delta t
\end{equation}

avec :

\begin{itemize}
    \item[$Q$ :] La quantité d'énergie conduite par l'objet en $[J]$.
    \item[$S$ :] Surface de l'objet en $[m^2]$.
    \item[$\alpha$ :] Le coéfficient de convection en  $[\frac{J}{m^2 \cdot K \cdot s}]$.
    \item[$T_{1}$ :] Température d'un côté de l'objet en $[K]$ ou $\degree C$.
    \item[$T_{2}$ :] Température de l'autre côté de l'objet en $[K]$ ou $\degree C$.
    \item[$\Delta t$ :] Durée de la convection en $[s]$.
\end{itemize}

\subsection{Excercice}
\subsubsection{Énnoncé}
Un homme se trouve en slip dans le froid.
Par seconde, il perd $ 120 [J] $.
Calculez la température extérieure.

Voici ce qu'on sait :

\[T_{1} = 32 \degree C\]
\[S = 1,6 [m^2]\]
\[\alpha = 7 [\frac{J}{m^2 \cdot K \cdot s}]\]

\subsubsection{Correction}
Comme on précise par secondes, ça veut dire que $ \Delta t = 1[s] $.
On peut donc remplir l'équation \ref{eq:4}.

\[120 = 1,6 \cdot \alpha \cdot (32 - T_{2}) \cdot 1\]

Il faut maintenant résoudre pour $ T_{2} $.

\[120 = 11,2(32 - T_{2})\]
\[120 = 358,4 - 11,2T_{2}\]
\[238,4 = 11,2T_{2}\]
\[T_{2} = 21,286[\degree C]\]

\section{Dilatation thermique}

\subsection{Théorie}

La dilatation thermique est l'expansion à pression constante du volume d'un
corps occasionné par son réchauffement.

Pour calculer la différence de longueur provoqué par le changement de température peut être calculé avec équation :

\begin{equation} \label{eq:diltherm}
    \Delta l = \alpha \cdot l_{i} \cdot (T_{f} - T_{i})
\end{equation}

avec :

\begin{itemize}
    \item[$\Delta l$ :] La différence de longueur en $[m]$.
    \item[$\alpha$ :] Le coefficient de dilatation linéaire en $[K^{-1}]$.
    \item[$l_{1}$ :] Longeur initiale de l'objet en $[m]$.
    \item[$T_{f}$ :] Température finale de l'objet en $[K]$ ou $\degree C$.
    \item[$T_{i}$ :] Température initiale de l'objet en $[K]$ ou $\degree C$.
\end{itemize}

La longeur finale peut être calculée avec les équations :

\begin{equation}
    l_{f} = l_{i} + \Delta l
\end{equation}

\begin{equation}
    l_{f} = l_{i} (1 + \alpha (T_{f} - T_{i}))
\end{equation}

\subsection{Exercice}

\subsubsection{Énoncé}

Calculez l'augementation de la hauteur de la tour eifel sachant :

\[\alpha_{fer} = 11 \cdot 10^{-6} [K^{-1}]\]
\[T_{i} = 0 \degree C\]
\[T_{f} = 50 \degree C\]
\[l_{i} = 80 [m]\]

\subsubsection{Correction}

On nous demande de calculer $\Delta l$, on peut donc utiliser l'équation \ref{eq:diltherm} :

\[\Delta l = 11 \cdot 10^{-6} \cdot 80 \cdot (50 - 0)\]
\[\Delta l = 0.044 [m]\]

\end{document}
