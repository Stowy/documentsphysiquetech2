= Balistique
Fabian Huber <fabian.hbr@eduge.ch>
v1.0 2020.08.30
:stem:
:toc:
:toc-title: Table des matières
:imagesdir: img

== Théorie

image::schemaBalistique.png[Schema balistique, 75%, 75%, align=center]

Le vecteur __asciimath:[vec "V"]__, qui représente la vitesse de l'objet, peut être représenté à l'aide de deux autres vecteurs : __Vx__ et __Vy__.

=== Équations

. asciimath:[cos(alpha) = frac{"Vx"}{"V"}]
. asciimath:[sin(alpha) = frac{"Vy"}{"V"}]
. asciimath:[x = "Vx" * t + x0]
. asciimath:[y = frac{1}{2} * g * t^2 + "V0y" * t + y0]
. asciimath:["Vy" = g * t + "V0y"]

=== Explications

* La balistique se simplifie en deux dimension.
* Si on omet le vent et la résistance de l'air, l'objet est en MRU (Mouvement Réctiligne Uniforme) sur l'axe horizontal. Ce qui veut dire que sa vitesse sur l'axe X (__Vx__) ne change pas et que seul __Vy__ change. C'est aussi pour ça que __Vx__ peut être écrit __V0x__.
* Selon ou l'on place le repère dans notre calcul, __g__ peut être négatif ou positif. (Si l'axe y est positif vers le haut, __g__ est négatif, sinon il est positif)
* __X__, __Y__ et __t__ doivent être au même point ! 

=== Exemple
image::schemaExemple.png[pdfwidth=50%]

Imaginons une fontaine dont l'eau sort du point A et arrive dans le bassin au point C. On aimerait savoir deux choses : la vitesse (asciimath:[vec V]) à la sortie de l'embout de l'eau (point *A* sur le schéma), puis sa vitesse à l'impact avec l'eau du bassin (point *C*).
Notre repère se trouve en bas à gauche.

==== Question 1
Quelle est la vitesse (asciimath:[vec V]) à la sortie de l'embout de l'eau (point *A* sur le schéma) ?

On peut déjà constater que l'angle de départ de l'eau est de 0 degré.
Si on applique la première équation, on peux également déduire que asciimath:["Vx" = V].

asciimath:[cos(0) = "Vx"/"V"]

asciimath:[1 = "Vx"/"V"]

asciimath:[1 * V = "Vx"]

asciimath:[V = "Vx"]

On peut également déduire que asciimath:["Vy" = 0].

asciimath:[sin(0) = "Vy"/"V"]

asciimath:[0 = "Vy"/"V"]

asciimath:[0 * V = "Vy"]

asciimath:["Vy" = 0 "m/s"]

Maintenant que nous avons Vy au niveau de l'embout, il nous faut __Vx__.
Comme __Vx__ ne change pas au cours de la chute, on peux le calculer au niveau du bassin. On peut commencer par calculer le temps que l'eau prends pour arriver au bassin grâce à la 3ème équation. Sauf que dans la 3ème équation nous avons 2 inconnues (__Vx__ et __t__) et comme nous avons __Vy__, __y__ et __y0__, on peut trouver __t__ dans la 4ème équation. Dans notre cas, __y__ vaut 0 car notre repère est en bas à droite et c'est là ou l'eau arrive. __y0__ est donc égale à 0.5m. Le placement du repère fait également que __g__ est négatif.

asciimath:[0 = 1/2 * g * t^2 + "Vy" * t + 0.5]

asciimath:[0 = 1/2 * -9.81 * t^2 + 0 * t + 0.5]

asciimath:[0 = -4.905 * t^2 + 0.5]

asciimath:[-0.5 = -4.905 * t^2]

asciimath:[frac{-0.5}{-4.905} = t^2]

asciimath:[0.5/4.905 = t^2]

asciimath:[sqrt(0.5/4.905) = t]

asciimath:[t ~~ 0.319"s"] ou asciimath:[t ~~ -0.319 "s"]

Comme on ne peut pas retourner dans le temps, c'est le __t__ positif qui est correct.
Maintenant que nous avons __t__, on peut utiliser la 3ème equation pour calculer __Vx__.

asciimath:[0.6 = "Vx" * 0.319 + 0]

asciimath:[0.6/0.319 = "Vx"]

asciimath:["Vx" ~~ 1.881"m/s" = 6,8 "km/h"]

Comme asciimath:["Vx" = V] on à donc notre réponse : asciimath:[vec V = ((1.881), (0))]

==== Question 2
Quelle est la vitesse et l'angle d'impact de l'eau dans le bassin ?

Pour la vitesse, on peut juste utiliser pythagore.

asciimath:["Vimpact" = sqrt(3,129^2 + 1,881^2) = 3,65"m/s" = 13,14"km/h"]

Pour l'angle, on peut faire un vecteur catésien avec __Vx__ et __Vy__ puis le convertire en polaire.
Déjà on sait que __Vx__ ne change pas, on à donc la première partie de notre vecteur. Ce que nous voulons est __Vy__ au niveau de l'impact. Pour ceci on peut utiliser la 5ème équation, comme nous connaissons __t__ et __V0y__.

asciimath:["Vy" = -9.81 * 0.319]

asciimath:["Vy" = -3.129]

asciimath:[vec V"impact" = ((1.881), (-3.129))]

On peut maintenant convertir ces coordonnées cartésiennes en coordonnées polaires ce qui nous permet d'avoir l'angle d'impact. Attention de bien avoir le bon angle (celui qui augemente dans le sens anti-horaire, donc beta dans ce cas).

asciimath:[alpha = |arctan(-3.129/1.881)| ~~ 58.988^@]

asciimath:[beta = 360 - 58 = 302^@]

<<<
== Exercices

<<<
== Solutions